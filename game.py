
# Prompt:
#    Ask for name
#    guess your birthday
#     guess number: name, were you born in month / year?
#     4 attempts, between 1924 to 2004


# import randint
from random import randint

# set variable guess number to 0
guess_number = 0

#ask for name
name = input("Hi! What is your name? ")

for bday in range(5):

  # randomize num for month
  month = str(randint(1, 12))

  # randomize num for year
  year = str(randint(1924, 2004))

  # prompt user if bday is correct
  print("Guess " + str(guess_number + 1) + ": " + name + " were you born in " + month + " / " + year + " ? ")
  prompt = "yes or no? "
  answer = input(prompt)

  #if statement
  if answer == "yes":
    print("I knew it!")
    break
  elif answer == "no":
    guess_number += 1

    if guess_number == 5:
      print("I have other things to do. Good bye")
    else:
      print("Drat! Lemme try again")

